# First Assignment - DevOps
First assignment PSW 2018

Team:
+ 808176 Marco Fagioli
+ 793679 Pietro Colombo

# Application
L'applicazione consiste in un programma Python che si occupa della gesione di una rubrica telefonica, tramite l'utilizzo di un database. L'applicazione tratta l'inserimento dei dati nella rubrica, dopo una precedente validazione, in un database implementato in MongoDB.

Per l'interfacciamento dell'applicazione Pyhton a MongoDB viene usata la libreria `pymongo`.

L'applicazione Python successivamente si occupa della validazione dei dati presi in input e, nel caso questi passino il test, vengono inseriti nel database.

L'applicazione controlla anche l'avvenuta connessione al database, oltre all'effettivo inserimento dei dati validati all'interno del istanza del database di mongoDB.

# DevOps
Lo sviluppo del progetto riguarda lo sviluppo degli aspetti della metodologia DevOps:

+ Containerization/Virtualization
+ Continuous Integration

## Containerization/Virtualization
La conteinerizzazione viene effettuata tramite l'uso della tecnologia docker, e vengono utilizzati tre container per fare eseguire due diverse `docker image`. Le immagini usate sono:

+ database: docker immage di mongo standard;
+ test: docker image personalizzata con script python per il test sulle funzioni che validano (usate dall'applicazione principale) i dati da inserire nel database, utilizzando come immagine di partenza Python 3.6.
+ applicazione: docker image personalizzata con immagine di partenza Python 3.6.

## Continuous Integration

Per sfruttare al meglio GitLab, il progetto prevede l'utilizzo della CI/CD, completamente integrata in GitLab.

Il file `.gitlab-ci.yml` continene i comandi che la pipeline va ad eseguire ad ogni push del nuovo codice. Abbiamo implementato differenti stage di lavoro in cui vengono automatizzate diverse operazioni:

+ stage `build_test` in cui vengono scaricate le immagini di partenza su cui montare le immagini personalizzate per il test delle funzioni e viene fatto il push su registry.
+ stage `test` in cui le docker image vengono runnate e si effettuano i test sulle funzioni di validazione dell'input.
+ stage `build_application` in cui vengono scaricate le immagini di partenza su cui montare le immagini personalizzate per l'applicazione e viene fatto il push su registry.
+ stage `deploy` n cui le docker image vengono runnate.

## Creazione docker image_test (stage: build_test)

La docker image dei test viene costruita dal `dockerfile.test` tramite il comando `docker build -t registry.gitlab.com/coich/first-assignment-devops/test -f Dockerfile.test .`

La quale immagine viene fatto il push su registry.
`docker push registry.gitlab.com/coich/first-assignment-devops/test`

## Test funzioni di validatione (stage: test_image)

Utilizzando l'immagine appena creata andiamo ad eseguirla insieme al container del database per verificare il corretto funzionamento delle funzioni che valutano l'imput e quelle che verificano la comunicazione con il database.

+ `docker run --name db-mongo -d --hostname=db_rubrica --net=bridge --expose=27017 mongo:latest`
+ `docker run --name test_app --link db-mongo:mongo -i registry.gitlab.com/coich/first-assignment-devops/test`

Se i test vanno a buon fine si passa alla fase di build dell'applicazione vera e propria.

## Creazione docker image_application (stage: build_application)

La docker image dell'applicazione viene costruita dal `dockerfile` tramite il comando `docker build -t registry.gitlab.com/coich/first-assignment-devops/app-rubrica -f Dockerfile .`

La quale immagine viene fatto il push su registry.
`docker push registry.gitlab.com/coich/first-assignment-devops/app-rubrica`

## Esecuzione docker image_application (stage: deploy)

L'esecuzione delle docker image viene fatta attraverso due comandi che runnano le due immagini.

+ `docker run --name db-mongo -d --hostname=db_rubrica --net=bridge --expose=27017 mongo:latest`

+ `docker run --name test_app --link db-mongo:mongo -i registry.gitlab.com/coich/first-assignment-devops/test`

Rispettivamente i comandi eseguono il container con database mongoDB in background e il conteiner dei test, che attraverso un link può comunicare con il DB esistente verificando il collegamento.
