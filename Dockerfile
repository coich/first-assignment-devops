FROM python:3.6

MAINTAINER pietrocolombo@me.com
MAINTAINER coich1996@gmail.com

RUN apt-get update
RUN apt-get install -y python3-pip
RUN pip3 install pymongo

WORKDIR /app
COPY . /app

CMD ["python", "./applicazione/main.py"]
