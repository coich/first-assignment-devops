import json
from validazione import valida_nome, valida_cognome, valida_telefono, valida_eta, valida_connessione

from pymongo import MongoClient

def test_name():
    names = ['pippo', 'prova$', 'marco maria', 'ludovica', 'fd JF2', 'piiiii', '3443 kfe .95' ]
    boolnames = [True, False, True, True, False, True, False]
    for name, boolname in zip(names, boolnames):
        try:
            assert valida_nome(name) == boolname
        except AssertionError:
            print('Error Assertion name')
            exit(1)

def test_surname():
    surnames = ['fagiolo', 'colombo', 'olly$baldi', 'radice vago', 'va31 ghi']
    boolsurnames = [True, True, False, True, False]
    for surname, boolsurname in zip(surnames, boolsurnames):
        try:
            assert valida_cognome(surname) == boolsurname
        except AssertionError:
            print('Error Assertion surname')
            exit(1)

def test_telephone():
    telephones = ['0101010101', '1234590', 'ABCDEABCDE', '0987654321', 'caio', '6789054321']
    booltelephones = [True, False, False, True, False, True]
    for telephone, booltelephone in zip(telephones, booltelephones):
        try:
            assert valida_telefono(telephone) == booltelephone
        except AssertionError:
            print('Error Assertion telephone')
            exit(1)

def test_age():
    ages = ['12', '-200', '24', '34', '832', '45', '-21', 'ciao']
    boolages = [True, False, True, True, False, True, False, False]
    for age, boolage in zip(ages, boolages):
        try:
            assert valida_eta(age) == boolage
        except AssertionError:
            print('Error Assertion age')
            exit(1)

def test_connection(client):
    try:
        assert valida_connessione(client) == True
    except AssertionError:
        print('Error Assertion connection')
        exit(1)

print("Testing")
test_name()
test_surname()
test_telephone()
test_age()
print("Fine testing validazione")

client = MongoClient('db_rubrica', 27017)

test_connection(client)
print("Fine testing")

client.drop_database('address_book')
db = client['address_book']
contacts_collection = db["contacts"]

contact = {"first_name" : 'pippo', \
		"last_name" : 'colombo', \
		"telephone" : '0123401234', \
		"age": '23' }
contacts_collection.insert_one(contact)
