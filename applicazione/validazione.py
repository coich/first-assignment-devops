def valida_nome(name):
    for i in range (0, len(name)):
        if not (name[i] >= 'a' and name[i] <= 'z') and not (name[i] >= 'A' and name[i] <= 'Z') and not name[i] == ' ':
            return False
    return True

def valida_cognome(surname):
    for i in range (0, len(surname)):
        if not (surname[i] >= 'a' and surname[i] <= 'z') and not (surname[i] >= 'A' and surname[i] <= 'Z') and not surname[i] == ' ':
            return False
    return True

def valida_telefono(number):
    if not len(number) == 10:
        return False
    for i in range (0, len(number)):
        if not (number[i] >= '0' and number[i] <= '9'):
            return False
    return True

def valida_eta(age):
    try:
        age = int(age)
    except:
        return False
    if age > 0 and age < 110:
        return True
    else:
        return False
    
def valida_connessione(clientDB):
    if clientDB is None:
        return False
    return True