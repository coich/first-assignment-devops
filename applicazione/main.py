import json
from validazione import valida_nome, valida_cognome, valida_telefono, valida_eta, valida_connessione
from pymongo import MongoClient

client = MongoClient('db_rubrica', 27017)
client.drop_database('address_book')
db = client['address_book']
contacts_collection = db["contacts"]

bases_contacts = json.load(open('applicazione/bases_contacts.json'))
for contact in bases_contacts.values():
	contacts_collection.insert_one(contact)

print("finito inserimento da file")